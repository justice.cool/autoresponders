import {injectable} from 'inversify';

@injectable()
export class IAutorespondersLogger {
    info(info: string) {
        console.log(info);
    }
}
