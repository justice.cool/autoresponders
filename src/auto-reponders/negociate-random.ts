import gql from 'graphql-tag';
import { injectable, inject } from 'inversify';
import { IAutorespondersLogger } from '../services';
import { JCoolApi, IHooks, HookPostBody, jcoolSchema as jc } from '@justice.cool/api-wrapper';
import moment from 'moment';


/**
 * An auto-responder that randomly accepts / rejects / negociates claims & facts
 */
@injectable()
export class NegociateRandomResponder implements IHooks {

    @inject(JCoolApi) protected api: JCoolApi;
    @inject(IAutorespondersLogger) private log: IAutorespondersLogger;

    /** Called when someone filed a new dispute against you */
    async newDispute(data: HookPostBody) {
        await this.mediationNegociation(data);
    }

    async mediationNegociation({ disputeId }: HookPostBody) {
        await this.reviewFacts(disputeId);
        await this.reviewClaims(disputeId);
    }

    private randomBool() {
        return Math.random() > 0.5;
    }

    private randomComment() {
        return this.randomBool() ? undefined : { html: 'I am a random robot, and I have been instructed to negociate this.' };
    }

    private async reviewFacts(disputeId: string) {

        // == get all facts that must be reviewed for the given dispute
        const { dispute } = await this.api.client.get(gql`query NR_GetFacts($id: String!) {
            dispute(id: $id) {
                facts(onlyWaitingMyReview: true) {
                    ref
                    valueType
                    history {
                        value
                        kind
                    }
                }
            }
        }
        `, { id: disputeId });


        if (!dispute.facts.length) {
            return;
        }

        // those facts have been contested by the opponent
        //  Thus, we cannot "accept" them as-it... we have to give our own version
        const withoutValue = dispute.facts.filter(x => x.history[0].kind === jc.DisputeFactHistoryKind.Contest);
        const withValue = dispute.facts.filter(x => x.history[0].kind === jc.DisputeFactHistoryKind.MyVersion);
        const reviews: jc.FactAmendmentInput[] = [
            // facts with no value => we're just gonna give the last version that had a value
            ...withoutValue.map<jc.FactAmendmentInput>(x => ({
                ref: x.ref,
                value: x.history.find(y => y.kind === jc.DisputeFactHistoryKind.MyVersion)?.value,
            })),
            // facts with value => random negociation
            ...withValue.filter(x => this.randomBool())
                .map<jc.FactAmendmentInput>(x => {
                    // depending on this fact type, build either another value, or reject the fact.
                    switch (x.valueType.primary) {
                        case 'bool':
                            return {
                                ref: x.ref,
                                value: !x.value,
                                comment: this.randomComment(),
                            };
                        case 'double':
                            return {
                                ref: x.ref,
                                value: x.value * (Math.random() + 0.5),
                                comment: this.randomComment(),
                            };
                        case 'int':
                            return {
                                ref: x.ref,
                                value: Math.round(x.value * (Math.random() + 0.5)),
                                comment: this.randomComment(),
                            };
                        case 'enum':
                            const opts = x.valueType.options;
                            if (opts instanceof Array) {
                                return {
                                    ref: x.ref,
                                    value: opts[Math.floor(Math.random() * opts.length)].value,
                                    comment: this.randomComment(),
                                }
                            }
                            break;
                        case 'date':
                            return {
                                ref: x.ref,
                                value: moment(x.value).add(-Math.random() * 3, 'months'),
                                comment: this.randomComment(),
                            }
                    }
                    return {
                        ref: x.ref,
                        comment: this.randomBool() ? undefined : { html: 'I am a random robot, and I have been instructed to reject this fact.' },
                    }
                }),
        ];


        this.log.info(`Accepting ${dispute.facts.length - reviews.length} facts, giving reviewing ${reviews.length} facts`);

        // == post amendments
        const result = await this.api.client.mutate(gql`mutation NR_ReviewFacts($id: String!, $reviews: [FactAmendmentInput!]!) {
            dispute(id: $id) {
                amendFacts(reviews: $reviews, behaviour: applyOrWait) {
                    missingInfoFromMe {
                        name
                    }
                }
            }
        }`, {
            id: disputeId,
            reviews,
        });

        // if some info is missing, our robot will not be able to fill it :(
        //   => post a message saying so
        const missing = result.dispute
            .amendFacts
            .missingInfoFromMe
            .map(x => `"${x.name}"`);
        if (missing.length) {
            await this.api.getDispute(disputeId).postMessage(`I am your robot servant, and I have been asked to answer new info: ${missing.join(', ')}...

            I am very sorry, but I dont know how to answer that: I have never seen this information.`)
        }
    }



    private async reviewClaims(disputeId: string) {

        // === get all claims to be reviewed
        const { dispute } = await this.api.client.get(gql`query NR_GetClaims($id: String!) {
            dispute(id: $id) {
                claims(onlyWaitingMyReview: true) {
                    id
                    lastProposition {
                        ...Prop
                    }
                    initialProposition {
                        ...Prop
                    }
                }
            }
        }
        fragment Prop on ClaimProposition {
            ...on IClaimProposition { status }
            ...on ClaimServiceProposition {
                service {
                    delay
                }
            }
            ...on ClaimCompensationProposition {
                compensation {
                    amount
                    currency
                }
            }
        }
        `, { id: disputeId });


        if (!dispute.claims.length) {
            return;
        }

        const rejects: jc.ClaimRejectionInput[] = [];
        const accepts: string[] = [];
        const negociates: jc.ClaimCounterPropositionInput[] = [];
        for (const c of dispute.claims) {
            // get a proposition that is not an abandon
            const prop: jc.ClaimProposition = c.lastProposition.status === jc.ClaimStatus.Abandoned
                ? c.initialProposition
                : c.lastProposition;

            // === negociate 50% of claims
            if (this.randomBool()) {
                if ('compensation' in prop) {
                    negociates.push({
                        claimId: c.id,
                        comment: this.randomComment(),
                        compensation: {
                            amount: prop.compensation.amount * (Math.random() + 0.5),
                            currency: prop.compensation.currency,
                        }
                    });
                    continue;
                }
                if ('service' in prop) {
                    negociates.push({
                        claimId: c.id,
                        comment: this.randomComment(),
                        service: {
                            delay: prop.service.delay * (Math.random() + 0.5),
                        }
                    })
                    continue;
                }
            }

            // === reject 25% of claims
            if (this.randomBool() && c.lastProposition.status !== jc.ClaimStatus.Abandoned) {
                rejects.push({
                    claimId: c.id,
                    comment: this.randomComment(),
                });
                continue;
            }

            // === accepts 25% of claims
            accepts.push(c.id);
        }


        this.log.info(`Accepting ${dispute.claims.length} claims`);

        // == post propositions
        await this.api.client.mutate(gql`mutation NR_AcceptDispute($id: String!, $accepts: [String!]!, $rejects: [ClaimRejectionInput!]!, $negociates: [ClaimCounterPropositionInput!]!) {
            dispute(id: $id) {
                acceptClaims(claims: $accepts)
                rejectClaims(reject: $rejects)
                counterProposeClaims(propositions: $negociates)
            }
        }`, {
            id: disputeId,
            accepts,
            rejects,
            negociates,
        });
    }
}