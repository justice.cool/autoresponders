import gql from 'graphql-tag';
import { injectable, inject } from 'inversify';
import { IAutorespondersLogger } from '../services';
import { HookPostBody, JCoolApi, IHooks, jcoolSchema as jc } from '@justice.cool/api-wrapper';


/**
 * An auto-responder that accepts all facts, but rejects all claims each time it is asked to
 */
@injectable()
export class RejectsClaimsResponder implements IHooks {

    @inject(JCoolApi) protected api: JCoolApi;
    @inject(IAutorespondersLogger) private log: IAutorespondersLogger;

    /** Called when someone filed a new dispute against you */
    async newDispute({disputeId}: HookPostBody) {

        // == get all facts & claims for the given dispute
        const {dispute} = await this.api.client.get(gql`query RC_GetDisputeClaims($id: String!) {
            dispute(id: $id) {
                claims (onlyWaitingMyReview: true) { id }
                facts { ref }
            }
        }
        `, {id: disputeId});

        this.log.info(`Accepting ${dispute.facts.length} facts, and rejecting ${dispute.claims.length} claims`);

        // == accept every fact
        const result = await this.api.client.mutate(gql`mutation RC_ReviewFacts($id: String!) {
            dispute(id: $id) {
                    amendFacts(reviews: [], behaviour: applyOrWait) {
                        missingInfoFromMe {
                            name
                        }
                    }
            }
        }`, {
            id: disputeId,
        });

        // if some info is missing, our robot will not be able to fill it :(
        //   => post a message saying so
        const missing = result.dispute
            .amendFacts
            .missingInfoFromMe
            .map(x => `"${x.name}"`);
        if (missing.length) {
            await this.api.getDispute(disputeId).postMessage(`I am your robot servant, and I have been asked to answer new info: ${missing.join(', ')}...

            I am very sorry, but I dont know how to answer that: I have never seen this information.`)
        }

        // == reject every claim
        await this.api.client.mutate(gql`mutation RC_RejectClaims($id: String!, $claims: [ClaimRejectionInput!]!) {
            dispute(id: $id) {
                rejectClaims(reject: $claims)
            }
        }`, {
            id: disputeId,
            claims: dispute.claims.map<jc.ClaimRejectionInput>(x => ({
                claimId: x.id,
                comment: {
                    html: 'I am a robot, and I have been told that I should reject this.',
                }
            })),
        });
    }
}