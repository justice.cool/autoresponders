import gql from 'graphql-tag';
import { injectable, inject } from 'inversify';
import { IAutorespondersLogger } from '../services';
import { JCoolApi, IHooks, HookPostBody, jcoolSchema as jc } from '@justice.cool/api-wrapper';


/**
 * An auto-responder that accepts everything each time it is asked to
 */
@injectable()
export class AcceptsAllResponder implements IHooks {

    @inject(JCoolApi) protected api: JCoolApi;
    @inject(IAutorespondersLogger) private log: IAutorespondersLogger;

    /** Called when someone filed a new dispute against you */
    async newDispute(data: HookPostBody) {
        await this.mediationNegociation(data);
    }

    async mediationNegociation({ disputeId }: HookPostBody) {
        await this.reviewFacts(disputeId);
        await this.reviewClaims(disputeId);
    }

    private async reviewFacts(disputeId: string) {

        // == get all facts that must be reviewed for the given dispute
        const { dispute } = await this.api.client.get(gql`query AA_GetFacts($id: String!) {
            dispute(id: $id) {
                facts(onlyWaitingMyReview: true) {
                    ref
                    history {
                        value
                        kind
                    }
                }
            }
        }
        `, { id: disputeId });


        if (!dispute.facts.length) {
            return;
        }

        // those facts have been contested by the opponent
        //  Thus, we cannot "accept" them as-it... we have to give our own version
        //   => we're just gonna give the last version that had a value
        const withoutValue = dispute.facts.filter(x => x.history[0].kind === jc.DisputeFactHistoryKind.Contest);
        const reviews: jc.FactAmendmentInput[] = withoutValue.map<jc.FactAmendmentInput>(x => ({
            ref: x.ref,
            value: x.history.find(y => y.kind === jc.DisputeFactHistoryKind.MyVersion)?.value,
        }));


        this.log.info(`Accepting ${dispute.facts.length - reviews.length} facts, giving last value for ${reviews.length} facts`);

        // == accept everything
        const result = await this.api.client.mutate(gql`mutation AA_ReviewFacts($id: String!, $reviews: [FactAmendmentInput!]!) {
                dispute(id: $id) {
                    amendFacts(reviews: $reviews, behaviour: applyOrWait) {
                        missingInfoFromMe {
                            name
                        }
                    }
                }
            }`, {
            id: disputeId,
            reviews,
        });

        // if some info is missing, our robot will not be able to fill it :(
        //   => post a message saying so
        const missing = result.dispute
            .amendFacts
            .missingInfoFromMe
            .map(x => `"${x.name}"`);
        if (missing.length) {
            await this.api.getDispute(disputeId).postMessage(`I am your robot servant, and I have been asked to answer new info: ${missing.join(', ')}...

            I am very sorry, but I dont know how to answer that: I have never seen this information.`)
        }
    }



    private async reviewClaims(disputeId: string) {

        // === get all claims to be reviewed
        const { dispute } = await this.api.client.get(gql`query AA_GetClaims($id: String!) {
            dispute(id: $id) {
                claims(onlyWaitingMyReview: true) {
                    id
                }
            }
        }
        `, { id: disputeId });


        if (!dispute.claims.length) {
            return;
        }

        this.log.info(`Accepting ${dispute.claims.length} claims`);

        // == accept everything
        await this.api.client.mutate(gql`mutation AA_AcceptDispute($id: String!, $claims: [String!]!) {
            dispute(id: $id) {
                acceptClaims(claims: $claims)
            }
        }`, {
            id: disputeId,
            claims: dispute.claims.map(x => x.id),
        });
    }
}