# Purpose

Each file in this directory is the implementation of one of Justice.cool API development  [Auto-Responders](https://docs.justice.cool/#/auto-responders).

You can use these as examples of naive implementations of Justice.cool API.

# Contributing

Feel free to contribute to auto-responders by creating pull requests in this repository.
