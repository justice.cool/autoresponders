const fs = require('fs');

/**
 * You can put your API keys in api-keys.json (which is ignored by git)
 *
 * NB: generate your api keys at https://app.staging.justice.cool/account/corporate)
 */
//
let apiKeys = {
    DEMANDER_API_KEY: null,
    OPPONENT_API_KEY: null,
};
if (!fs.existsSync('api-keys.json'))
    throw new Error('Please specify your API keys in api-keys.json');
apiKeys = JSON.parse(fs.readFileSync('api-keys.json', 'utf-8'));


// extract api uri from codegen-user.yml
const gqlCfg = fs.readFileSync('codegen-user.yml', 'utf8');
const [API_URI] = /http(s?):.+$/m.exec(gqlCfg);

// export config
module.exports = {
    API_URI,
    ...apiKeys,
};