This repository demonstrates via mocha unit tests how to implement different justice.cool API scenarii.

It can be used as support material [to the documentation](https://docs.justice.cool)

# Content of this repository

## test/**

These are test scenarii to demonstrate how to implement a specific use case

## src/services/**

Contains services that can be used accross this repository

## src/auto-responders/**

When testing the API, you can chose test opponents to be [auto responders](https://docs.justice.cool/#/auto-responders).
They are fake automated opponents, that will always have the same behaviour.

Those opponents are defined in this repository so you can inspect their behaviour in details, or get inspiration on how to implement justice.cool api.

# Installation

- Use your favorite editor (ex: [vscode](https://code.visualstudio.com/))
- Install required extensions to support mocha unit tests (for vscode, you can use [test explorer ui](https://marketplace.visualstudio.com/items?itemName=hbenl.vscode-test-explorer) with [mocha test explorer](https://marketplace.visualstudio.com/items?itemName=hbenl.vscode-mocha-test-adapter) )
- If using vscode, optionally install [Apollo GraphQL](https://marketplace.visualstudio.com/items?itemName=apollographql.vscode-apollo) to enable auto completion and syntax highlighting of GraphQL requests.

```
git clone git@gitlab.com:justice.cool/public-api-demos.git

cd public-api-demos

npm install

code public-api-demos
```

You will then see unit tests corresponding to different scenarii. Identify those that correspond to your use case using  [the documentation](https://docs.justice.cool).


# Run unit tests

To run tests of this repository, you **must** provide your API key. See instructions in config.js file.