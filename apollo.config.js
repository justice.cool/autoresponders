const {API_URI} = require('./config');

module.exports = {
    client: {
      service: {
          url: API_URI,
      },
      includes: [
          "src/**/*.{ts,tsx,js,jsx,graphql,gql}"
      ]
    },
  };