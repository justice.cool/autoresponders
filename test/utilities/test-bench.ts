import gql from 'graphql-tag';
import { expect, assert } from 'chai';
import { API_URI, OPPONENT_API_KEY, DEMANDER_API_KEY } from '../../config';
import { HookPostBody, JCoolApi, jcoolSchema } from '@justice.cool/api-wrapper';

interface DisputeDb {
    lastHook?: string;
}

export type HookProcessor = (m: HookPostBody) => any | Promise<any>;

class BenchBase {

    api: JCoolApi;


    /** Sends a message on the dispute newsfeed */
    async sendMessage(dispute: string, text: string) {
        await this.api.client.mutate(gql`mutation PostMessage($dispute: String!, $text: String!) {
            dispute(id: $dispute) {
                postMessage(message: $text) {
                    id
                }
            }
        }`, {dispute, text});
    }
}

export class OpponentBench extends BenchBase {

    constructor() {
        super();
        this.api = new JCoolApi(OPPONENT_API_KEY, {}, API_URI);
    }

}

/**
 * Utilty wrapper around the justice.cool GraphQL API.
 */
export class TestBench extends BenchBase {

    api: JCoolApi;

    private processedHooks = new Set<string>();
    private processingHooks = new Set<string>();
    private disputes: {[key: string]: DisputeDb } = {};

    private hooks: {[key: string]: HookProcessor } = {};


    /** Some methods that helps you simulate your opponent behaviour  */
    opponent: OpponentBench;

    constructor() {
        super();
        this.api = new JCoolApi(DEMANDER_API_KEY, {}, API_URI);
        this.opponent = new OpponentBench();
    }

    async createDispute(data: jcoolSchema.DisputeCreationInput) {

        const {createDispute} = await this.api.client.mutate(gql`mutation CreateDispute($data: DisputeCreationInput){
            createDispute (data: $data) {
                id
                DEV_OPPONENT_FORM
            }
        }`, { data });

        this.disputes[createDispute.id] = {};
        return createDispute;
    }

    /** Registers a hook function */
    hook(hook: jcoolSchema.HookName, processor: HookProcessor) {
        this.hooks[hook] = processor;
        return this;
    }

    /**
     * Expect a hook to be called
     * @returns the hook data
     */
    async expectHook(disputeId: string, hook: jcoolSchema.HookName) {
        const backup = this.hooks;
        this.hooks = {};
        try {
            let ret: HookPostBody;
            this.hook(hook, msg => ret = msg);
            await this.pollHooks(disputeId);
            assert.exists(ret, 'Hook not called: ' + hook);
            return ret;
        } finally {
            this.hooks = backup;
        }
    }

    /**
     * Hooks are not reachable from a local-development environment.
     * Thus, we have to poll messages in order to simulate hook calls.
     */
    async pollHooks(disputeId: string) {
        const total: jcoolSchema.HookMessage[] = [];
        const db = this.disputes[disputeId];
        if (!db) {
            assert.fail('Dispute not referenced locally');
        }

        // hooks->list only returns the 50 first hook messages
        // (thus the 'while' loop, just in case more than 50 messages were queued)
        while (true) {

            // => fetch hooks
            // Here, we are
            //  - filtering on dispute id
            //  - getting already processed hooks.
            //  - getting only hooks after the last one that we've seen
            //
            // This is just in case you have "real" hooks that are catching those messages in your staging environment,
            // but the best practice in a real environment would be not to do so (would retreive hooks for all your disputes).
            //
            // NB 1: You MUST specify a hook url for hooks to be pused to queue.
            // NB 2: If you specified an invalid http(s) address as url, that tells justice.cool to queue hooks as already processed.
            //   => You must do this if want to implement a polling-only scenario (not recommanded in production).
            const {hooks}  = await this.api.client.get(gql`query PollHooks($dispute: String!, $after: String) {
                hooks {
                    list (disputes: [$dispute], after: $after, getProcessed: true) {
                        hookId
                        data
                        hookName
                        retryCount
                        entity {
                            ...on IEntity {
                                id
                                type
                                externalId
                            }
                        }
                    }
                }
            }`, {
                dispute: disputeId,
                after: db.lastHook
            });

            // If no hooks left, stop the loop.
            if (!hooks.list.length)
                break;

            // Process messages
            for (const h of hooks.list)
                await this.postHook({
                    time: '',
                    data: h.data,
                    hookId: h.hookId,
                    hookName: h.hookName,
                    retryCount: h.retryCount,
                    disputeId: h.entity.id,
                    entity: {
                        externalId: h.entity.externalId,
                        id: h.entity.id,
                        type: h.entity.type,
                    },
                });

            // Mark those hooks as processed
            // When polling, this is required to let justice.cool know that we have successully processed those hook messages.
            // When implementing "real" hooks, this step is automatically performed when your hook answers a "HTTP 200" status code.
            await this.api.client.mutate(gql`mutation MarkHooks($hooks: [String!]!) {
                hooks {
                    markProcessed(hookIds: $hooks)
                }
            }`, {hooks: hooks.list.map(x => x.hookId)});

            // update the "last processed hook"
            db.lastHook = hooks.list[hooks.list.length - 1].hookId;
            total.push(...hooks.list);
        }
        return total;
    }

    /**
     * Processes a hook message.
     * This method is the equivalent of the "POST" you'll have to implement with "real" hooks.
     * @argument body would be exactly what justice.cool would POST you as "body" (JSON format) when implementing "real" hooks.
     */
    private async postHook(body: HookPostBody) {
        // It is recommanded to implement idempotence, for safety (in case justice.cool pushes you the same message twice).
        // This would be the most basic pattern of idempotence.
        if (this.processedHooks.has(body.hookId))
            return;

        // If you use polling AND real hooks, beware of race conditions when justice.cool is posting you the hook while you are processing it though polling.
        // This is the purpose of the "processingHooks" pattern below, not actually useful in the current situation.
        if (this.processingHooks.has(body.hookId))
            throw new Error('Already processing');

        try {
            // the actual hook processing
            await this.transactedHookProcessing(body);
            // add to "processed" hooks (see above)
            this.processedHooks.add(body.hookId);
        } finally {
            // remove from "processing" hooks (see above)
            this.processingHooks.delete(body.hookId);
        }
    }

    /** This would be the equivalent of your actual hook processing, that you should wrap in a DB transaction (if applicable, in case something fails) */
    private async transactedHookProcessing(body: HookPostBody) {
        // just forward the call to the test
        const hook = this.hooks[body.hookName];
        if (!hook)
            assert.fail('Not expecting hook ' + body.hookName);
        await hook(body);
    }
}