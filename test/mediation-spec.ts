import 'reflect-metadata';
import 'mocha';
import { expect, assert } from 'chai';
import { equal } from 'assert';
import gql from 'graphql-tag';
import moment from 'moment';
import { TestBench } from './utilities/test-bench';
import {ulid} from 'ulid';
import { jcoolSchema as jc } from '@justice.cool/api-wrapper';


describe ('demander companies', () => {

    let jcool: TestBench;

    beforeEach(() => {
        jcool = new TestBench();
    })

    it ('can receive a message', async () => {

        // create a dispute
        const {id, myId, form, DEV_OPPONENT_FORM} = await createDispute();
        assert.notExists(form, 'should not have any form (complete dispute)');

        // check no hooks have been called (no hook registered => will throw if a hook is called)
        await jcool.pollHooks(id);

        // simulate the opponent sending a mesage
        await jcool.sendMessage(id, 'Hi, here is some <b>message</b>');


        // check that we receive a hook
        const hook = await jcool.expectHook(id, jc.HookName.Message);
        expect(hook.disputeId).to.equal(id);
        expect(hook.entity.externalId).to.equal(myId);
        expect(hook.data.text).to.equal('Hi, here is some <b>message</b>');
        expect(hook.data.html).to.equal('Hi, here is some &lt;b&gt;message&lt;/b&gt;'); // 'html' version is safe to display as html (no scripts, no css styles, ...)
    })


    /** Creates a dispute */
    async function createDispute() {
        const myId = 'EXT-ID-' + ulid();
        const ret = await jcool.createDispute({
                features: [],
                updateMode: jc.UpdateMode.Auto,
                signatureMode: jc.SignatureMode.Auto,
                externalId: myId,
                facts: [{
                    variable: 'litigationType',
                    answer: 'work'
                }, {
                    variable: 'categoryOfYourProblem',
                    answer: 'fired'
                }, {
                    variable: 'typeOfDismissal',
                    answer: 'dismissalForSimpleFault'
                }, {
                    variable: 'dateOfNotificationOfDismissal',
                    answer: moment().add(-5, 'days').toDate(),
                }, {
                    variable: 'priorNotice',
                    answer: false,
                }, {
                    variable: 'dateOfTheFault',
                    answer: moment().add(-15, 'days').toDate()
                }, {
                    variable: 'ageOfTheApplicant',
                    answer: 35
                }, {
                    variable: 'typeOfFault',
                    answer: 'blablah',
                }, {
                    variable: 'layoff',
                    answer: false,
                }, {
                    variable: 'startDateOfYourEmploymentContract',
                    answer: moment().add(-400, 'days').toDate(),
                }, {
                    variable: 'justificationOfDismissal',
                    answer: false,
                }, {
                    variable: 'protectedEmployee',
                    answer: false
                }, {
                    variable: 'collectiveLicensing',
                    answer: false,
                }, {
                    variable: 'haveYouEverBeenSubjectToSimilarSanctions',
                    answer: false,
                }, {
                    variable: 'levelOfResponsibility',
                    answer: 'moderateLiability'
                }, {
                    variable: 'socialRightClaimCategory',
                    answer: 'financialCompensation'
                }, {
                    variable: 'doYouHaveANoticePeriod',
                    answer: false,
                }, {
                    variable: 'isItWanted',
                    answer: false,
                }, {
                    variable: 'leaveLeft',
                    answer: true,
                }, {
                    variable: 'numberOfLeavesLeft',
                    answer: 25
                },
                {
                    variable: 'howManyDaysOfVacationsAreYouEntitledToPerYear',
                    answer: 30
                }, {
                    variable: 'conventionCollective',
                    answer: 'insurance',
                },


                // those variables are "private" (tied to a demander)
                // but you can specify them globaly (i.e. not on the demander) when there is only one demander.
                {
                    variable: 'positionHeld',
                    answer: 'managerialStaff'
                },
                {
                    variable: 'howAreYouPaid',
                    answer: ['salary'],
                }, {
                    variable: 'monthlySalary',
                    answer: 2500
                }
                ],
                demanders: [{
                    person: {
                        firstName: 'Perh',
                        lastName: 'Sohn',
                    },
                }],
                opponent: {
                    // opponent is a company
                    company: {
                        identifier: '80314744600022',
                    },
                    // when opponent is a company, contact means is optional
                    contactMeans: [
                        // you can specify this to tell justice.cool to also contact company as it is used to
                        { auto: true },
                        // specifying this, justice.cool will also send a "recommandé électronique" to this address to contact them:
                        { email: 'perh.sohn@nobody.com' }
                    ]
                }
            });
        return {
            ...ret,
            myId,
        };
    }
});